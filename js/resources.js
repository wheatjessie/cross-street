//image loading utility
(function () {
    let resourceCache = {},
        loading = [],
        readyCallbacks = [];

    function load(images) {
        images.forEach(  url => _load(url) )
    }

    //cache loading images
    function _load(url) {
        if (resourceCache[url]) {
            return resourceCache[url];
        } else {
            let img = new Image();
            img.onload =  () => {
                resourceCache[url] = img;
                //console.log(resourceCache );
                if (isReady()) {
                    readyCallbacks.forEach(function (func) { func(); });
                }
            };
            resourceCache[url] = false;
            img.src = url;
        }
    }

    //get image from cache
    function get(url) {
        return resourceCache[url];
    }

    //check if all images loaded
    function isReady() {
        let ready = true;
        for (let k in resourceCache) {
            if (resourceCache.hasOwnProperty(k) &&
                !resourceCache[k]) {
                ready = false;
            }
        }
        return ready;
    }

    //add  function to the callback stack when ready
    function onReady(func) {
        readyCallbacks.push(func);
    }

    //lobal Resources object.
    window.Resources = {
        load: load,
        get: get,
        onReady: onReady,
        isReady: isReady
    };
})();
