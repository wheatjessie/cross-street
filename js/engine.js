
(function (global) {

    const doc = global.document,
        win = global.window,
        btnPause = doc.querySelector('#pause'),
        btnRestart = doc.querySelector('#restart'),
        btnSetting = doc.querySelector('#setting'),
        info = doc.querySelector('#info'),
        infoScore = doc.querySelector('#score'),

        canvas = doc.createElement('canvas'),
        gameWidth = 500,
        gameHeight = 600,

        tiles = {
            stone: 'images/stone-block.png',
            road: 'images/road-block.png',
            grass: 'images/grass-block.png'
        },
        chars = {
            boy: 'images/char-boy.png',
            girl: 'images/char-girl.png'
        }
        cars = {
            car1: 'images/car-yellow.png',
            car2: 'images/car-red.png',
            bus: 'images/car-bus-blue.png',
            truck: 'images/car-truck-white.png',
            sport: 'images/car-sport-grey.png'
        },
        images =
            Object.values(tiles).concat(Object.values(chars), Object.values(cars));

    let ctx = canvas.getContext('2d'),
        flag = true,
        collision = true,
        score = 0,
        status = true,
        animation,
        player,
        allEnemies,
        lastTime;


    class Moveobj {
        constructor(sprite) {
            this.sprite = sprite;
        }

        render() {
            let img = Resources.get(this.sprite);
            this.width = img.width;
            this.height = img.height;
            ctx.drawImage(img, this.x, this.y);            
        }
    }

    class Enemy extends Moveobj {
        constructor(sprite = cars.car1, speed = 100) {
            super(sprite);
          
            this.x = -400 - this.width + Math.floor(Math.random() * 300);
            this.y = 100 * Math.floor(Math.random() * 4) + 180 - this.height;
            this.s = speed + Math.random() * 500;
        }

        update(dt) {
            if (this.x < gameWidth) {
                this.x = this.x + dt * this.s;
            } else {
                this.x = -400 - this.width + Math.floor(Math.random() * 300);
                this.y = 100 * Math.floor(Math.random() * 4) + 180 - this.height;
                this.s = 100 + Math.random() * 600;
                // console.log(this.x,this.y)
            }
        }
    }


    class Player extends Moveobj {
        constructor(sprite = chars.boy) {
            super(sprite);
            this.x = 200;
            this.y = 0;
            console.log(this);
        }

        update() {
            let step = 8;
            switch (this.direct) {
                case 'left':
                    this.x - step > 0 && this.x - step < gameWidth - this.width ? this.x = this.x - step : this.x = this.x;
                    break;
                case 'right':
                    this.x + step > 0 && this.x + step < gameWidth - this.width ? this.x = this.x + step : this.x = this.x;
                    break;
                case 'up':
                    this.y - step > 0 && this.y - step < gameHeight - this.height ? this.y = this.y - step : this.y = this.y;
                    break;
                case 'down':
                    this.y + step > 0 && this.y + step < gameHeight - this.height ? this.y = this.y + step : this.y = this.y;
                    break;
            }
            this.direct = '';
        }

        handleInput(direct) {
            this.direct = direct
        }
    }

    canvas.width = gameWidth;
    canvas.height = gameHeight;
    doc.body.appendChild(canvas);

    function init() {
        setPlayer();
        setEnemies();
        lastTime = Date.now();
        main();
        animation = win.requestAnimationFrame(main);
    }

    function setPlayer() {
       player = new Player(chars.girl);
    }

    function setEnemies() {
        allEnemies = [
            new Enemy(cars.car1),
            new Enemy(cars.car2),
            new Enemy(cars.bus),
            new Enemy(cars.sport),
            new Enemy(cars.truck)
        ]
    }

    function main() {
        var now = Date.now(),
            dt = (now - lastTime) / 1000.0;
        if (flag && collision && status) {
            update(dt);
            render();
            //win.cancelAnimationFrame(animation);
        } else if (!collision) {
            renderText('Game Over!');
        }
        else if (!status) {
            renderText('Awesome!');
        }
        lastTime = now;
        win.requestAnimationFrame(main);
    }

    function update(dt) {
        updateEntities(dt);
        checkCollisions();
        checkStatus();
    }

    function updateEntities(dt) {
        allEnemies.forEach(function (enemy) {
            enemy.update(dt);
        });
        player.update();
    }

    function checkCollisions() {
        for (let i = 0; i < allEnemies.length; i++) {
            var enemy = allEnemies[i];
            if (
                player.x > enemy.x + 15
                && player.x < (enemy.x + enemy.width * 0.8)
                && player.y > enemy.y - player.height * 0.5
                && player.y < (enemy.y + enemy.height * 0.7)
            ) {
                collision = false;
                break;
            }
        }
    }


    function checkStatus() {
        if (player.y > gameHeight - 100) {
            status = false;
            score++;
            infoScore.textContent = score;
        }
    }


    function render() {
        //render playground by tiling 
        const tile = 100,
            rowImages = [
                tiles.stone, tiles.road, tiles.road, tiles.road, tiles.road, tiles.grass
            ],
            numRows = Math.ceil(gameHeight / tile),
            numCols = Math.ceil(gameWidth / tile);

        for (let row = 0; row < numRows; row++) {
            for (let col = 0; col < numCols; col++) {
                var img = Resources.get(rowImages[row]);
                ctx.drawImage(img, col * tile, row * tile);
            }
        }
        //render player and enenmies
        renderEntities();
    }

    function renderEntities() {
        allEnemies.forEach(function (enemy) {
            enemy.render();
        });
        player.render();
    }

    function renderText(text) {
        ctx.fillStyle = 'rgb(255,0,0)';
        ctx.strokeStyle = 'rgb(50,50,50)';
        ctx.lineWidth = 5;
        ctx.font = 'bold 80px impact';
        ctx.textAlign = 'center';
        textBaseline = 'middle';
        ctx.strokeText(text, gameWidth / 2, gameHeight / 2);
        ctx.fillText(text, gameWidth / 2, gameHeight / 2);
    }

    function reset() {
        flag = true;
        collision = true;
        status = true;
        setPlayer();
        setEnemies();
    }

    function pause() {
        flag ? btnPause.textContent = 'Continue' : btnPause.textContent = 'Pause'
        return flag = !flag
    }

    Resources.load(images);
    Resources.onReady(init);

    global.ctx = ctx;

    doc.addEventListener('keydown', function (e) {
        var allowedKeys = {
            37: 'left',
            38: 'up',
            39: 'right',
            40: 'down'
        };
        player.handleInput(allowedKeys[e.keyCode]);
    });

    btnRestart.addEventListener('click', reset);
    btnPause.addEventListener('click', pause);

})(this);

